# Minecraft - Mushlantis

Mushlantis is a series of Minecraft Bedrock (Windows 10, PE, Xbox One, Nintendo Switch) worlds set in a mushroom biome. This project has all the screenshots, backups, and plans. We've hosted it on a website (http://minecraft.acord.org.au) using Jekyl

# Recent updates
* May 14 2019 - We started an underground trophy room using armor stands and daylight sensors. Backup coming on the 18th
* May 30 2019 - Full Diamond Armor

This repo will also house other ACORD worlds